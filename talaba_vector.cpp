#include "iostream"
#include "vector"
#include "algorithm"
using namespace std;
namespace student{
    class Student{
    public:
        string first_name,last_name,group;
        int age,course;
        Student(string first_name_,string last_name_, string group_,int course_,int age_){
            first_name=first_name_,last_name=last_name_,group=group_,course=course_,age=age_;
        }
        void print(){
            cout << first_name << " | "<< last_name <<" | " << group << " | " << course << " | "  << age << endl;
        }

    };
}
bool myFunc(student::Student student1,student::Student student2){
    return student1.first_name < student2.first_name;
}


int main(){
    vector<student::Student> students;
    int n;
    cout << "How many students are in your group:"; cin >> n;

    for(int i = 0; i < n; i++){

        string first_name,last_name,group;
        int age,course;
        cout << "Enter First Name:"; cin >> first_name;
        cout << "Enter Last Name:"; cin >> last_name;
        cout << "Enter Group:"; cin >> group;
        cout << "Enter Course:"; cin >> course;
        cout << "Enter Age:";cin >> age;
        student::Student student(first_name,last_name,group,course,age);
        students.push_back(student);
    }
    cout << "First Name  | " << "Last Name | " << "Group | " << "Course | " << "Age | \n";
    student::Student student1 = students[0];
    sort(students.begin(),students.end(), myFunc);
    for(student::Student student:students){
        student.print();
    }
    
}
